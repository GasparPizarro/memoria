\chapter*{Anexos}
\addcontentsline{toc}{chapter}{Anexos}
	\section*{A. Interfaz a ROS del cliente implementado}
		\addcontentsline{toc}{section}{A. Interfaz a ROS del cliente implementado}
		En esta sección se muestra la interfaz del radar a ROS. Todos los nombres en esta sección, de tópicos, servicios y parámetros, comienzan con el prefijo \texttt{/acumine\_radar/}. Los tópicos y servicios se muestran con su nombre, el tipo del mensaje o servicio (definidos en las carpetas \texttt{msg} y \texttt{srv} respectivamente) y una descripción de éstos.
		\subsection*{Tópicos publicados}
			La definicion de los mensajes publicados se encuentra en la carpeta \texttt{msg} del paquete.
			\begin{itemize}
				\item \texttt{radar/peak: Sight} \\
					Medidas del radar en los distintos modos de \textit{peaks}.
				\item \texttt{radar/spectrum: Spectrum} \\
					Medidas del radar en modo de espectro en frecuencia.
				\item \texttt{radar/analog\_signal: AnalogSignal} \\
					Medidas del radar en modo análogo.
				\item \texttt{scanner/pose: ScannerPose} \\ 
					Información de pose del \textit{scanner}.
				\item \texttt{scanning\_radar/peak: PeakPose} \\ 
					Medidas conjuntas de radar y \textit{scanner} en los distintos modos de \textit{peaks}.
				\item \texttt{scanning\_radar/spectrum: SpectrumPose} \\
					Medidas conjuntas de radar y \textit{scanner} en modo de espectro en frecuencia.
				\item \texttt{clouds: PointCloud2} \\
					Nubes de puntos con datos del radar girando.
				\item \texttt{processed\_clouds: PointCloud2} \\
					Nubes de puntos de datos procesados con el detector CFAR.
			\end{itemize}

		\subsection*{Servicios ofrecidos}
			La definición de los servicios se encuentra en la carpeta \texttt{srv} del paquete.
			\begin{itemize}
				\item \texttt{radar/set\_fft\_parameters: SetFFTParameters} \\ 
					Define los parámetros de la transformada de Fourier que aplica el radar.
				\item \texttt{radar/set\_processing\_mode: SetProcessingMode} \\ 
					Define el modo de procesamiento del radar.
				\item \texttt{scanner/enable\_scanner: EnableScanner} \\
					Activa o desactiva el \textit{scanner}.
				\item \texttt{scanner/set\_scanner\_trajectory: SetScannerTrajectory} \\
					Define la posición o la velocidad del \textit{scanner}.
				\item \texttt{scanner/set\_pid\_parameters: SetPIDParameter} \\
					Define las ganancias del controlador PID del \textit{scanner}.
			\end{itemize}
		
		\subsection*{Parámetros}
			Los parámetros que se definen para el uso del radar están definidos en el archivo\\  \texttt{config/acumine\_radar.yaml} y es necesario que estén todos definidos antes de iniciar los nodos de la librería.
			\begin{itemize}
				\item \texttt{ip} : 
					IP del computador del radar.
				\item \texttt{maximum\_range}: 
					Rango máximo del radar.
				\item \texttt{minimum\_range}:
					Rango mínimo del radar.
				\item \texttt{radar/port}:
					Puerto TCP asociado al \texttt{RadarServer}.
				\item \texttt{radar/fft\_parámeters}: 
					Parámetros de la transformada de Fourier del radar.
				\item \texttt{radar/processing\_mode}: 
					Modo de procesamiento del radar.
				\item \texttt{scanner/port}:
					Puerto TCP del \texttt{ScannerServer}.
				\item \texttt{scanner/pid\_parameters}: 
					Parámetros del controlador PID del \textit{scanner}.
				\item \texttt{scanner/trajectory}: 
					Posición y velocidad del \textit{scanner}.
				\item \texttt{scanning\_radar/port}: 
					Puerto TCP del \texttt{ScanningRadarServer}.
			\end{itemize}

	\section*{B. Diferencias encontradas entre \cite{martinez2012} y el verdadero protocolo del radar}
		\addcontentsline{toc}{section}{B. Diferencias encontradas entre \cite{martinez2012} y el verdadero protocolo del radar}
		A lo largo del desarrollo de la memoria, se encontraron diferencias entre \cite{martinez2012} y el protocolo de comunicación del radar usado en esta memoria. Los siguientes son los mensajes que se diferencian de \cite{martinez2012}.
		\begin{itemize}
			\item \textbf{Estructura general}
			\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,label={-}]
				\item Cabecera: \texttt{0xB255}
				\item \textit{Timestamp} en segundos: 8 bytes, entero sin signo
				\item Nanosegundos: 4 bytes, entero sin signo
				\item Largo de mensaje: 4 bytes, entero sin signo
				\item Datos: Largo variable, definido por el campo anterior
				\item \textit{Checksum}: 2 bytes, entero sin signo
				\item Cola: \texttt{0x759F}
			\end{itemize}

			\item \textbf{Mensajes de peaks} \\
				Este tipo de mensaje es el que se encontró en vez de los definidos como \textit{RadarHighestIntensityMessage}, \textit{RadarLowRangePeakMessage}, \textit{RadarHighRangePeakMessage} y \textit{RadarAllPeaksMessage}, siendo solo el identificador de mensaje el campo que diferencia los cuatro tipos de mensajes.
			\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,label={-}]
				\item Identificador de mensaje: 1 byte, entero sin signo
				\item \textit{Timestamp}: 8 bytes, entero sin signo
				\item Rango máximo: 4 bytes, punto flotante
				\item Resolución: 4 bytes, punto flotante
				\item Numero de \textit{peaks}: 1 byte, entero sin signo
				\item \textit{Peaks}:
					\begin{itemize}[label={-}]
						\item Rango: 4 bytes, punto flotante
						\item Intensidad: 4 bytes, punto flotante
					\end{itemize}
			\end{itemize}
			\item \textbf{\textit{ScannerTrajectoryControlMessage}}
				\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,label={-}]
					\item Identificador de mensaje: 1 byte, entero sin signo
					\item Número de eje: 1 byte, entero sin signo
					\item \textit{TrajectorySetPoint}
				\end{itemize}
			\item \textbf{\textit{TrajectorySetPoint}}
				\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,label={-}]
					\item Identificador de comando: 1 byte, entero sin signo
					\item Posicion: 4 bytes, punto flotante
					\item Rapidez: 4 bytes, punto flotante
					\item Aceleración: 4 bytes, punto flotante
				\end{itemize}
			\item \textbf{\textit{ScannerEnabledControlMessage}}
				\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,label={-}]
					\item Identificador de mensaje: 1 byte, entero sin signo
					\item Número de eje: 1 byte, entero sin signo
					\item Activación: 1 byte, entero sin signo
				\end{itemize}	
			\item \textbf{\textit{ScannerPIDSettingsMessage}}
				\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,label={-}]
					\item Identificador de mensaje: 1 byte, entero sin signo
					\item Número de eje: 1 byte, entero sin signo
					\item Ganancia proporcional: 2 bytes, entero sin signo
					\item Ganancia derivativa: 2 bytes, entero sin signo
					\item Ganancia integral: 2 bytes, entero sin signo
					\item Límite de integral: 2 bytes, entero sin signo
				\end{itemize}	
		\end{itemize}
