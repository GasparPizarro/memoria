function s = fmmod (m, fc, fs, freqdev)
  l = length (m);
  t = 0:1./fs:(l-1)./fs;
  int_m = cumsum (m)./fs;

  s = cos (2*pi.*fc.*t + 2*pi.*freqdev.*int_m);
end
