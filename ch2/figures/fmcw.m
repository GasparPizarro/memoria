Fs = 4000;
t = 0:Fs-1'/Fs; % Sampling times
dev = 50; % Frequency deviation in modulated signal
x = [linspace(10,100,2000) linspace(10,100,2000)];
y = fmmod(x,10,Fs,0.5);
subplot(2, 1, 1);
plot(y);
subplot(2, 1, 2);
plot(x);
subplot(2, 1, 1);
title("Se\-nales producidas por un radar FMCW", "interpreter", "tex");
ylabel("Potencia [W]");
subplot(2, 1, 2);
ylabel("Frecuencia [Hz]");
xlabel("Tiempo [s]");

