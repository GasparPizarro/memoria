\chapter{Antecedentes}
	En este capítulo se describen las tecnologías usadas para esta memoria, a nivel de hardware, correspondiente al radar y sus características, como a nivel de software, esto es la plataforma ROS y sus funcionalidades ofrecidas para el desarrollo.

	\section{Radar}
		RADAR, acrónimo de \textit{Radio Detection and Ranging}\footnote{A pesar de ser un acrónimo, RADAR será utilizado como una palabra cualquiera de aquí en adelante.} es una tecnología de percepción basada en la emisión de ondas electromagnéticas, específicamente ondas de radio ($30$ \si{\mega\hertz} - $300$ \si{\giga\hertz}) que permite la detección de obstáculos, además de medición de distancias y velocidades. 
		
		El principio en el que se basa el radar es que cuando una onda pasa por un medio que cambia de densidad, por ejemplo aire y madera, una porción de la onda pasa al otro lado y la otra se refleja, como muestra la Figura \ref{radar_reflexion}.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=8cm]{ch2/figures/radar_reflexion.pdf}
			\caption{\small{Principio de funcionamiento del radar. Al cambiar de medio, una onda emitida (línea negra), se divide en dos: Una onda reflejada (línea roja) y una onda transmitida (línea gris).}}
			\label{radar_reflexion}
		\end{figure}
		
		Las ondas reflejadas son recibidas por el radar y esto permite obtener información sobre la línea que ha recorrido la onda y, previo procesamiento de los datos, se puede obtener un conjunto de medidas de potencia recibida v/s distancia, que indican si a cierta distancia está o no un objeto, como muestra la Figura \ref{radar}.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=7cm]{ch2/figures/radar_medidas.png}
			\caption{Mediciones tomadas por un radar en una habitación rectangular. Se puede ver que los máximos en cada medición angular forman un rectángulo, mostrando las paredes del entorno. Tomado de \cite{ma}.}
			\label{radar}
		\end{figure}
		
		Los radares actualmente tienen aplicaciones civiles y comerciales en los campos de:
		\begin{itemize}
			\item Minería
			\item Vehículos inteligentes
			\item Localización
			\item Navegación marítima
		\end{itemize}
		entre otros \cite[sección 1.3]{ma2012}.
		
		\subsection{Medición de distancias con radar}
			\label{radar_medicion}
			Las dos técnicas de medición de distancias con radar más populares son las siguientes:
				\subsubsection{Tiempo de vuelo (Time-of-flight, TOF)}
					Esta técnica consiste en la emisión de pulsos electromagnéticos desde el radar, y la distancia se mide proporcional al tiempo entre el envío y recepción de éstos, con la ecuación \ref{radar_tof_ecuacion}.
					\begin{equation}
						r_i = \frac{cT_i}{2}
						\label{radar_tof_ecuacion}
					\end{equation}
					donde $r_i$ es la distancia entre el radar y el objeto $i$, $c$ es la velocidad de la luz y $T_i$ es el tiempo entre que se envía un pulso y el radar recibe un eco. El diagrama de tiempos que muestra el funcionamiento de la técnica se muestra en la Figura \ref{radar_tof}.
					\begin{figure}[H]
						\centering
						\includegraphics[width=9cm]{ch2/figures/radar_tof.pdf}
						\caption{Diagrama de tiempo de la técnica de tiempo de vuelo. El tiempo transcurre de arriba hacia abajo y las distancias se miden de izquierda a derecha. Primero, el radar envía un pulso, el cual se refleja y transmite en el obstáculo 1. Luego se refleja en el obstáculo 2. Finalmente, el radar recibe los pulsos reflejados, con los que se calcula la distancia usando la ecuación \ref{radar_tof_ecuacion}.}
						\label{radar_tof}
					\end{figure}
					
					El principal problema de esta técnica es que, al ser necesario enviar pulsos, la electrónica de transmisión y recepción del radar tiene que poder manejar grandes potencias, del orden de los $4$ \si{\kilo\watt}, y poder procesar las señales enviadas a altas velocidades. Además, con este método se tiene una precisión que no sirve para aplicaciones de localización \cite[sección 2.6.1]{ma2012}.
				
				\subsubsection{Onda continua modulada en frecuencia (FMCW)}
					Esta técnica consiste en la emisión continua de una onda, con una frecuencia portadora $f_c$, la que es modulada por una curva de dientes de sierra, y la distancia se mide de forma inversamente proporcional a la diferencia entre la frecuencia de la onda enviada y la frecuencia de la onda recibida. Las señales modulada y de frecuencia se muestran en la Figura \ref{radar_fmcw_funcionamiento}.
					\begin{figure}[H]
						\centering
						\includegraphics[width=9cm]{ch2/figures/radar_fmcw_funcionamiento.png}
						\caption{Ondas generadas en un radar FMCW. El gráfico superior muestra la señal que emite el radar, y el gráfico inferior muestra la frecuencia de esta señal. En este caso la frecuencia de la onda portadora es de 10 \si{\hertz}.}
						\label{radar_fmcw_funcionamiento}
					\end{figure}
					Entonces la distancia se mide con la ecuación \ref{radar_fmcw_ecuacion}.
					\begin{equation}
						r_i = \frac{cT_d}{2\Delta f}{f_b}_i
						\label{radar_fmcw_ecuacion}
					\end{equation}
					donde $r_i$ es la distancia entre el radar y el objeto $i$, $\Delta f$ es la amplitud del barrido de frecuencias del radar, $T_d$ es el periodo de la curva de frecuencia y ${f_b}_i$ es la diferencia entre la frecuencia recibida por el radar proveniente del objeto $i$ y la frecuencia portadora $f_c$. La interacción entre estas variables se muestra en la Figura \ref{radar_fmcw}.
					\begin{figure}[H]
						\centering
						\includegraphics[width=10cm]{ch2/figures/radar_fmcw.pdf}
						\caption{Interacción entre las variables asociadas en la ecuación \ref{radar_fmcw_ecuacion}.}
						\label{radar_fmcw}
					\end{figure}
					
		
		\subsection{Medidas en un radar y pre-procesamiento}
			\label{radar_pre_procesamiento}
			Con los métodos referidos anteriormente en \ref{radar_medicion} es posible obtener, para una dirección determinada, un conjunto de medidas de potencia recibida, llamada un \textit{A-scope}, que se denota de la siguiente manera:
			\begin{equation}
				S^{lin} = [S^{lin}_1, S^{lin}_2, \dots , S^{lin}_Q]
			\end{equation}
			
			Con esto, el rango del radar se discretiza en $Q$ secciones o \textit{bins} y para cada una de ellas se tiene la potencia recibida en esa sección. Esta potencia se mide en \si{Watts}, pero usualmente $S^{lin}$ es una medición sin unidades, ya que las medidas son relativas a la potencia transmitida o cualquier otra potencia \cite[sección 2.2]{ma2012}. Por otro lado, cuando hay un obstáculo, la potencia recibida por un radar decae con la cuarta potencia de la distancia a la que está éste, es decir
			\begin{eqnarray}
				S^{lin}_i &\propto & \frac{\Gamma^{RCS}}{r_i^4}\\
				S^{lin}_i &=& K\frac{\Gamma^{RCS}}{r_i^4}
				\label{radar_medida}
			\end{eqnarray}
			Donde $S^{lin}_i$ es la medición del radar en la sección $i$ y $\Gamma^{RCS}$ es llamada Sección Transversal de Radar, o \textit{Radar Cross Section} (RCS), una propiedad que encapsula las propiedades físicas del obstáculo, siendo algo como la ``brillantez'' de éste ante el radar. Un problema con esto es que la RCS puede variar con rangos muy grandes dependiendo de la distancia a la que está un obstáculo y el ángulo desde el cual se ve, lo que hace necesario un preprocesamiento de los datos recibidos por el radar para que estos puedan ser procesados por un computador.
			
			\subsubsection{Compresión logarítmica}
				Se toma el logaritmo de la señal recibida por el radar (ecuación \ref{radar_medida}), obteniéndose lo siguiente:
				\begin{eqnarray}
					S^{log}_i &=& 10log_{10}\left(S^{lin}\right) = 10log_{10}\left( K\frac{\Gamma^{RCS}}{r_i^4} \right)\\
					S^{log}_i &=& 10log_{10}(K)+10log_{10}\left(\Gamma^{RCS}\right)-40log_{10}(r_i)
					\label{radar_cl}
				\end{eqnarray}
				
				Con esto la dependencia del logaritmo de la señal recibida se vuelve lineal con el logaritmo de la distancia. Nótese que a partir de este punto las medidas dejan de ser en Watts (o adimensionales, si se usan potencias relativas), sino que son en decibeles.
			\subsubsection{Compensación de rango}
				En los radares FMCW se tiene que la distancia de un obstáculo es proporcional a la frecuencia recibida por el radar, es decir 
				\begin{equation}
					{f_b}_i = K_fr_i
				\end{equation}
				
				Entonces, se puede aplicar un filtro pasa alto respecto a la frecuencia, con lo que de la ecuación \ref{radar_cl} se tiene:
				\begin{eqnarray}
					S^{log\_ rc}_i &=& S^{log}_i + 40log_{10}({f_b}_i) \quad \quad\text{[40 \si{\deci\bel}/década]} \nonumber \\
					S^{log\_ rc}_i &=& 10log_{10}(K)+10log_{10}(\Gamma^{RCS})-\cancel{40log_{10}(r_i)} + 40log_{10}(K_f)+ \cancel{40log_{10}(r_i)} \nonumber \\
					S^{log\_ rc}_i &=& K^*+10log_{10}(\Gamma^{RCS})
				\end{eqnarray}
				con $K^*$ una constante. Entonces la señal recibida se vuelve independiente de la distancia a la que esté el obstáculo, quedando solo dependiente de la RCS.
				
			Los preprocesamientos descritos hacen, finalmente, que blancos iguales sean vistos de igual forma, independientemente de la distancia hacia el radar. Un problema con estos métodos de preprocesamiento es que funcionan cuando las mediciones corresponden a blancos reales, ya que todas las ecuaciones anteriores involucran la RCS en $\Gamma^{RCS}$. Sin embargo, para las mediciones que no corresponden a obstáculos, sino que solamente corresponden a ruido, los métodos de preprocesamiento resultan en una señal cuyo valor aumenta con la distancia, lo cual puede causar problemas para la detección de obstáculos.
				
		\subsection{Post-procesamiento de mediciones}

			Después de hacer el preprocesamiento de las mediciones como se describe en la sección \ref{radar_pre_procesamiento}, se tiene una curva que se puede interpretar como la potencia ``vista'' por el radar en función de la distancia. Lo que se debe hacer con esta señal es decidir, para cada punto de ésta, si lo que se está viendo es un obstáculo o solo ruido. Para ello se puede hacer lo siguiente:

			\subsubsection{Aplicación de umbral fijo}
				Una forma directa para detectar obstáculos es establecer un umbral, y asumir que las mediciones que superan ese umbral son obstáculos y las mediciones bajo éste son ruido. A pesar de ser simple de implementar, esta técnica necesita definir el umbral a priori, lo que puede causar que para cierto umbral ciertas mediciones sean detectadas como obstáculos mientras que con otro umbral sean descartadas como ruido, no habiendo una forma intuitiva o matemáticamente justificada para elegir el umbral. Además, al no modelar la señal probabilísticamente, no se puede obtener información acerca de los valores mismos de la señal, es decir, no se puede saber qué diferencia hay entre dos puntos que superan el umbral establecido, y no toman en cuenta la incertidumbre respecto a la RCS, con lo que éste método es propenso a fallas \cite[sección 3.3.1]{ma2012}.

			\subsubsection{Aplicación de umbral adaptativo}
				\label{radar_cfar}
				Este enfoque asume que cada medida del radar es una muestra de una variable aleatoria cuya distribución de probabilidad depende de la presencia o ausencia de objetos en el rango que se está midiendo, esto es, para cada medición $S$ se asumen dos hipótesis:
				\begin{itemize}
					\item $S$ proviene del ruido ambiental ($H_0$). En este caso, se asume que el ruido tiene una distribución exponencial, esto es:
						\begin{equation}
							\label{radar_h0}
							P(S|H_0) = \left\{
							\begin{array}{ll}
								\lambda \exp(-\lambda S) & {\rm si\ } S\geq 0 \\
								0 & {\rm si\ } S<0 
								\end{array}
							\right.
						\end{equation}
					\item $S$ proviene de un obstáculo ($H_1$). En este caso, se asume que la medición sigue una distribución de Rayleigh, esto es:
						\begin{equation}
							\label{radar_h1}
							P(S|H_1) = \frac{x}{\sigma^2} \exp\left(-\frac{x^2}{2\sigma^2}\right)
						\end{equation}
				\end{itemize}
				Con estas dos distribuciones, se puede establecer un umbral que garantice una cierta probabilidad de falsa alarma (Constant False Alarm Rate, CFAR), esto es que el sistema decida que hay un obstáculo cuando solo se está leyendo ruido, $P_{FA}^{CFAR}$, dada por
				\begin{equation}
					S^{CFAR} = -\lambda^{-1} \ln\left(P_{FA}^{CFAR}\right)
				\end{equation}

				El problema que este método presenta al ser aplicado directamente es que, al igual que la aplicación de umbral fijo, necesita parámetros definidos a priori ($\lambda$ y $\sigma$). La forma de manejar este problema es haciendo una estimación adaptativa de los parámetros, usando los mismos datos. En términos generales, estos algoritmos (conocidos como \textit{procesadores}) consisten en que para una medición $S_i$ se aplica un test estadístico $T$, y si la razón $\frac{S_i}{T(S_i)}$ supera un umbral $\gamma$, se dice que la medición corresponde a un obstáculo. Diferentes modelos probabilísticos de obstáculos y ruido, a la vez que diferentes esquemas de procesamiento, definen distintos $T$ y $\gamma$. Un caso es el procesador de promedio de celdas (\textit{Cell Averaging} CFAR, CA-CFAR). Este procesador tiene los siguientes parámetros:
				\begin{itemize}
					\item Una probabilidad de falsa alarma $P_{FA}^{CFAR}$.
					\item Un ancho de ventana de referencia $W$.
					\item Un ancho de ventana de guardia $G$.
				\end{itemize}
				Entonces, para cada medición $S_i$ el test $T$ se define como
				\begin{equation}
					T(S_i) = \frac{1}{K}\sum_{k=1}^{k=K}S_{i_k}
				\end{equation}
					Donde $\{S_{i_1}, S_{i_2}, \cdots, S_{i_k}\}$ son las mediciones que quedan	espacialmente en el anillo definido por $G$ y $W$ en torno a $S_i$. Con distribuciones de probabilidad para ruido y obstáculos como en las ecuaciones \ref{radar_h0} y \ref{radar_h1} respectivamente, el umbral $\gamma$ es
					\begin{equation}
						\gamma = K\left[\left({P_{FA}^{CFAR}}\right)^\frac{-1}{K} - 1\right]
					\end{equation}

					y la probabilidad de detección se estima como
					\begin{equation}
						P_D^{CFAR}(S_i) = \left[1 + \frac{\gamma}{K(1 + \frac{S_i}{T(S_i)})}\right]^{-K}
					\end{equation}
				
				De esta forma, el procesador CA-CFAR (referido en adelante solamente como CFAR), hace que si una medida es alta, pero está rodeada de otras medidas altas, se tome como ruido, pero si las medidas que la rodean son bajas, se detecte como un obstáculo.
					
	\section{Robot Operating System}
	\label{robot_operating_system}

		\textit{Robot Operating System} (ROS) es un meta-sistema operativo desarrollado por la empresa Willow Garage\footnote{\url{http://willowgarage.com}} para el desarrollo de software para robots, que ofrece comunicación entre procesos, \textit{drivers} para distintos sensores y actuadores, y un sistema de compilación, construcción y manejo de paquetes\footnote{Es por esto que se le llama un meta-sistema operativo a ROS, porque las funciones descritas son propias de un sistema operativo, sin embargo ROS es un sistema que debe correr sobre un sistema operativo normal.}. Actualmente esta plataforma corre sobre sistemas Unix, principalmente sobre Ubuntu y Mac OS, y, aunque es posible, no se ha desarrollado completamente una adaptación a Windows\cite[Sección 3]{ros:intro}.
		
		ROS es un sistema que ha sido pensado para la reutilización de código\cite[Sección 2]{ros:intro}, esto es que, por ejemplo, un algoritmo de reconocimiento de imágenes pueda ser portado fácilmente de un robot a otro, al organizar los programas hechos en \textit{paquetes}, que pueden funcionar independientemente, o incluir dependencias de otros paquetes. Por otro lado, existe una comunidad abierta de desarrollos en torno a ROS\footnote{\url{http://www.ros.org/browse/list.php}}, en la que se pueden encontrar paquetes para diversas funcionalidades.
		
		ROS es un sistema distribuido, cuyos programas se comunican mediante la red, usando un protocolo propio del sistema, de especificación abierta \cite{ros:rpc}, por lo tanto éstos pueden ser programados en cualquier lenguaje. Actualmente existen librerías clientes que permiten la programación en lenguajes C++, Python y Lisp, mientras que se tienen librerías experimentales para programar en lenguajes Java y Lua. Esto permite que en un sistema o paquete, por ejemplo, secciones de alto desempeño puedan ser programadas en un lenguaje eficiente como C++, mientras que interfaces gráficas u operaciones de alto nivel puedan ser programadas en un lenguaje más simple y rico en librerías como Python.
		
		Algunos conceptos básicos sobre el funcionamiento de ROS se muestran a continuación.
		\begin{description}
			\item[Nodo:] Un nodo es un programa ejecutable en ROS, que puede estar dedicado a una función específica, como leer de un sensor, procesar datos, etc. Estos nodos pueden comunicarse con otros con las herramientas que ofrece el sistema.
			\item[Mensaje:] Un mensaje es un dato, con una estructura inamovible y rigurosamente definida, que puede transmitirse entre los nodos de ROS. Estos mensajes se declaran en archivos de texto simple, en un lenguaje de dominio específico para estos mensajes, fuera del código de los nodos, mientras que las herramientas de compilación generan las estructuras de datos o clases para poder ser usados en los nodos. Así se posibilita la comunicación de mensajes entre nodos escritos en distintos lenguajes.
			\item[Tópico:] Junto con los servicios, los tópicos son una de las formas estándares de comunicación entre nodos de ROS, que permite que los nodos se comuniquen con mensajes, descritos anteriormente. Esta forma de comunicación se basa en el patrón de publicadores y suscriptores\cite{gof1995}, que permiten que un nodo pueda suscribirse, es decir obtener mensajes, de varios tópicos, y que varios nodos puedan publicar en el mismo tópico, es decir que un nodo escuchando un tópico está recibiendo mensaje de más de un nodo.
			\item[Servicio:] Un servicio es una forma de comunicación síncrona entre nodos, que se comunican usando pares de mensajes, un mensaje con los datos de petición de servicio (por ejemplo, dos enteros), y otro con el dato de respuesta del servicio, (por ejemplo, si el servicio es sumar dos enteros, el tipo de dato de respuesta sería un entero). Esta forma de comunicación es uno a uno, esto es que la respuesta a un servicio se entrega a un solo nodo (aunque un servicio puede ser llamado por distintos nodos), al contrario de los tópicos, y permite una comunicación transparente entre nodos remotos.
		\end{description}
	
		ROS también ofrece herramientas para el desarrollo y despliegue de aplicaciones, que se muestran a continuación.
		
		\subsection{Servidor de parámetros}
			ROS provee un servicio para guardar parámetros, que permite que distintos nodos puedan compartir información, y que se pueda tener un estado común para un sistema\footnote{Con sistema, de aquí en adelante, se hace referencia a un conjunto de nodos corriendo simultáneamente, ya sean de un solo paquete, o nodos de distintos paquetes.}. Este servidor puede ser accedido mediante el programa \texttt{rosparam} en línea de comandos, o con las librerías clientes, como las de Python y C++. Con esto, un nodo puede definir un parámetro en el servidor de parámetros, y así éste se vuelve disponible para todos los demás nodos.

		\subsection{Roslaunch}
			Roslaunch es una herramienta de ROS que permite la ejecución de múltiples nodos con una sola llamada, a la vez que definir parámetros y reiniciar procesos terminados. Roslaunch usa archivos XML o YAML (lanzadores), de extensión \texttt{.launch} para definir nodos a correr, parámetros para cargar en el servidor de parámetros, e incluso otros archivos \texttt{.launch} para correr. Esto permite hacer que en un sistema compuesto por muchos nodos que ofrecen distintas funcionalidades, se puedan tener archivos \texttt{.launch} que permitan correr fácilmente distintas funcionalidades del sistema, por ejemplo, correr los nodos que hacen procesamiento de datos, o correrlos con una interfaz gráfica programada en otro nodo.

		\subsection{Rviz y Rqt\_plot}
			Rviz y Rqt\_plot son herramientas de ROS para visualización de datos. Rviz\cite{ros:rviz} permite la visualización de datos de sensores, modelos de movimiento de robots, y otros datos tridimensionales en una vista espacial. Rqt\_plot\cite{ros:rqt_plot} es una herramienta para la visualización de datos escalares, que permite de forma rápida tener mediciones de valores en función del tiempo. Ambas herramientas funcionan suscribiéndose a tópicos, de forma que funcionan transparentemente en un sistema, pudiéndose iniciar como nodos en un lanzador al igual que cualquier otro nodo.		
			
		\subsection{Rosbag}	
			Rosbag es una herramienta de ROS que permite guardar mensajes en archivos, permitiendo la reproducción del estado de un sistema posteriormente. Esto permite en un sistema separar el proceso de adquisición de datos del de procesamiento, grabando los datos con Rosbag para adquirir los datos y luego reproduciéndolos. De esta forma un nodo de procesamiento no ve diferencia entre los datos adquiridos directamente (con un sensor, en este caso el radar) de los datos reproducidos con Rosbag.

	\section{Equipos utilizados}
		\subsection{Radar}
			Este es el equipo más importante usado en este trabajo, y consiste en un radar Acumine de 94 \si{\giga\hertz}. Éste es un radar FMCW y consiste en un dispositivo sensor (emisor y receptor de radio y espejo director) y un computador que procesa los datos obtenidos por el sensor. El sensor se muestra en la Figura \ref{radar_hardware}.
			\begin{figure}[H]
				\centering
				\includegraphics[width=10cm]{ch2/figures/radar_hardware.pdf}
				\caption{Radar Acumine.}
				\label{radar_hardware}
			\end{figure}

			Este sensor consta de dos subsistemas, uno para el sistema emisor-receptor y otro para el motor del espejo, y estos se controlan independientemente. En términos lógicos, el radar, en particular el computador del radar, funciona con el sistema operativo QNX, y ofrece tres servidores TCP, que se muestran a continuación.
			\begin{description}
				\item[\texttt{RadarServer}:] Este servidor recibe comandos que controlan el procesamiento de los datos del radar, como los modos de procesamiento de la señal recibida o los parámetros de la transformada de Fourier que se le aplica a la señal, y entrega los datos de la señal recibida por el radar.
				\item[\texttt{ScannerServer}:] Este servidor recibe comandos que controlan el sensor del radar, como los parámetros del controlador PID\cite{ogata2001} del espejo del radar o la pose del espejo rotatorio\footnote{Pose se refiere al ángulo relativo del espejo con respecto a alguna recta de visión del radar.}, y entrega datos acerca de la pose del radar.
				\item[\texttt{ScanningRadarServer}:] Este servidor solo entrega datos del radar, combinando los datos de mediciones del radar con los de la pose de éste, realizando extrapolaciones para, dado el \textit{timestamp} de los datos que está recibiendo el radar, calcular la pose del \textit{scanner} y crear un mensaje conjunto.
			\end{description}
		
			La interfaz lógica del radar se muestra en la Figura \ref{radar_interfaz}.
			\begin{figure}[H]
				\centering
				\includegraphics[width=12cm]{ch2/figures/radar_interfaz.png}
				\caption{Interfaz lógica ofrecida por el radar \cite{martinez2012}.}
				\label{radar_interfaz}
			\end{figure}
		
			En \cite{martinez2012} está especificada la estructura de los mensajes recibidos y enviados por el radar, con lo que es posible implementar clientes que se comuniquen con el radar usando esos mensajes y con los que se puedan hacer otros procesamientos de los datos.

			Los distintos servidores del radar envían paquetes con la estructura general que se muestra en la Figura \ref{protocolo}.
			\begin{figure}[H]
				\centering
				\includegraphics[width=\textwidth]{ch2/figures/protocolo.png}
				\caption{Estructura general de los mensajes enviados por el radar.}
				\label{protocolo}
			\end{figure}
	
			La estructura de la Figura \ref{protocolo} tiene algunas diferencias con \cite{martinez2012}, como por ejemplo las definiciones de la cabecera y de la cola. Esta estructura fue descubierta analizando los paquetes que envía el radar\footnote{Esto se realizó previo a esta memoria, no por el autor.}.

			Las especificaciones técnicas del radar relevantes para este trabajo se muestran en la Tabla \ref{radar_specs}.
			\begin{table}[H]
				\centering
				\begin{tabular}{ll}
					\hline
					\textbf{Frec. portadora} & 94 \si{\giga\hertz}\\
					\hline
					\textbf{Potencia transmitida} & 10 \si{\milli\watt}\\
					\hline
					\textbf{Rango de operación} & Entre 1 \si{\metre} y 200 \si{\metre}\\
					\hline
					\textbf{Frec. de giro} & Entre 0 y 5 \si{\hertz}\\
					\hline
					\textbf{Conexión} & Ethernet\\
					\hline
				\end{tabular}
				\caption{Especificaciones técnicas del radar utilizado.}
				\label{radar_specs}
			\end{table}
		\subsection{Láser}
			Para comparar el radar con otros sensores se usó un sensor láser LD-LRS1000, de SICK, que se muestra en la Figura \ref{laser_hardware}. Este es un sensor láser giratorio, que detecta presencia o ausencia de obstáculos. Este sensor tiene interfaz a ROS, de forma que sus mediciones se pueden ver directamente con Rviz. Sus especificaciones se muestran en la Tabla \ref{laser_specs}.
			\begin{multicols}{2}
				\begin{table}[H]
					\centering
					\begin{tabular}{ll}
						\hline
						\textbf{Frec. de emisión} & $3,3126 \cdot 10^{14}$ \si{\hertz} (905 \si{\nano\metre})\\
						\hline
						\textbf{Res. angular} & \ang{0,0625}\\
						\hline
						\textbf{Campo de visión} & \ang{360}\\
						\hline
						\textbf{Frec. de giro} & Entre 5 \si{\hertz} y 10 \si{\hertz}\\
						\hline
						\textbf{Rango de operación} & Entre 0,5 \si{\metre} y 250 \si{\metre}\\
						\hline
						\textbf{Conexión} & Ethernet\\
						\hline
					\end{tabular}
					\caption{Especificaciones técnicas del sensor láser utilizado.}
					\label{laser_specs}
				\end{table}
				\begin{figure}[H]
					\centering
					\includegraphics[width=4cm]{ch2/figures/laser_hardware.png}
					\caption{Sensor láser SICK.}
					\label{laser_hardware}
				\end{figure}
			\end{multicols}

		\subsection{Base móvil}
			También se usó una base móvil para mover los sensores a los puntos de experimentación, la que se muestra en la Figura \ref{husky_hardware}. Esta es una base móvil Husky, de Clearpath Robotics. Éste es un vehículo orientado al movimiento en terrenos difíciles (tierra, pasto, lodo) y para el montaje rápido de cargas útiles (en este caso, los sensores), además de proveer energía para éstos. Este dispositivo tiene una interfaz directa a ROS, permitiendo su control desde un computador y pudiendo entregar mediciones como el estado de las baterías, odometría, etc. Sus especificaciones técnicas se muestran en la Tabla \ref{husky_specs}.
			\begin{multicols}{2}
				\begin{table}[H]
					\centering
					\begin{tabular}{ll}
						\hline
						\textbf{Masa} & 50 \si{\kilogram}\\
						\hline
						\textbf{Carga máxima} & 75 \si{\kilogram}\\
						\hline
						\textbf{Rapidez máxima} & 1 \si{\metre\per\second}\\
						\hline
						\textbf{Autonomía} & Entre 3 \si{\hour} y 8 \si{\hour}\\
						\hline
						\textbf{Conexión} & RS-232\footnotemark[1]\\
						\hline
					\end{tabular}
					\caption{Especificaciones técnicas de la base móvil utilizada.}
					\label{husky_specs}
				\end{table}
				\begin{figure}[H]
					\centering
					\includegraphics[width=5cm]{ch2/figures/husky_hardware.png}
					\caption{Base móvil Husky.}
					\label{husky_hardware}
				\end{figure}
			\end{multicols}
		\footnotetext[1]{No obstante, la base móvil se conectó al computador principal usando un adaptador a USB.}
	\section{Experiencias similares y el aporte de esta memoria}
		En \cite{chin2009} se muestra la implementación de una librería de control para un radar Acumine. Ese radar, a pesar de no ser idéntico al que se usa en esta memoria, funciona de la misma forma que el radar de ésta, con comunicación por protocolo TCP. El software producido en ese trabajo de título es un cliente con interfaz gráfica construida sobre la librería XView que permite controlar los parámetros del radar, como la resolución radial y el controlador del espejo, entre otros, además de mostrar los datos obtenidos por el radar y/o guardar los datos en un archivo.
		
		El aporte de esta memoria, comparada con \cite{chin2009}, será el habilitar el radar para su uso con la plataforma ROS, permitiendo de esta forma la compatibilidad del radar con otros dispositivos que se dispongan en el laboratorio (compatibles con ROS), como puede ser otros sensores o actuadores, y permitiendo la reutilización de código de la comunidad de ROS.
