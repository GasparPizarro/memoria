\select@language {spanish}
\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {i}}{section*.1}
\contentsline {chapter}{Agradecimientos}{\es@scroman {iii}}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos del proyecto}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivo general}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Objetivos espec\IeC {\'\i }ficos}{4}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Estructura de la memoria}{5}{section.1.3}
\contentsline {chapter}{\numberline {2}Antecedentes}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Radar}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Medici\IeC {\'o}n de distancias con radar}{7}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.1}Tiempo de vuelo (Time-of-flight, TOF)}{7}{subsubsection.2.1.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.2}Onda continua modulada en frecuencia (FMCW)}{8}{subsubsection.2.1.1.2}
\contentsline {subsection}{\numberline {2.1.2}Medidas en un radar y pre-procesamiento}{10}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Compresi\IeC {\'o}n logar\IeC {\'\i }tmica}{10}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}Compensaci\IeC {\'o}n de rango}{11}{subsubsection.2.1.2.2}
\contentsline {subsection}{\numberline {2.1.3}Post-procesamiento de mediciones}{11}{subsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.3.1}Aplicaci\IeC {\'o}n de umbral fijo}{11}{subsubsection.2.1.3.1}
\contentsline {subsubsection}{\numberline {2.1.3.2}Aplicaci\IeC {\'o}n de umbral adaptativo}{12}{subsubsection.2.1.3.2}
\contentsline {section}{\numberline {2.2}Robot Operating System}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Servidor de par\IeC {\'a}metros}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Roslaunch}{15}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Rviz y Rqt\_plot}{15}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Rosbag}{15}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Equipos utilizados}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Radar}{16}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}L\IeC {\'a}ser}{18}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Base m\IeC {\'o}vil}{19}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Experiencias similares y el aporte de esta memoria}{19}{section.2.4}
\contentsline {chapter}{\numberline {3}Dise\IeC {\~n}o e implementaci\IeC {\'o}n}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}Requerimientos}{20}{section.3.1}
\contentsline {section}{\numberline {3.2}Decisiones de dise\IeC {\~n}o}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Lenguajes de programaci\IeC {\'o}n}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Interfaz a ROS}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Interfaz gr\IeC {\'a}fica}{22}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Visualizaci\IeC {\'o}n de datos}{22}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Estructura general del software}{22}{section.3.3}
\contentsline {section}{\numberline {3.4}Decisiones incidentales}{24}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Lectura y escritura de mensajes}{24}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}\textit {Sockets} y sincronizaci\IeC {\'o}n}{24}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Robustez de la conexi\IeC {\'o}n}{25}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Par\IeC {\'a}metros del radar y configuraci\IeC {\'o}n}{25}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Medici\IeC {\'o}n de datos desde el radar}{25}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Funcionalidades implementadas}{26}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Nodos de comunicaciones}{26}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Interfaz gr\IeC {\'a}fica}{27}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Nubes de puntos}{28}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Detecci\IeC {\'o}n de obst\IeC {\'a}culos}{29}{subsection.3.5.4}
\contentsline {subsection}{\numberline {3.5.5}Lanzadores}{29}{subsection.3.5.5}
\contentsline {section}{\numberline {3.6}Modos de uso del sistema}{30}{section.3.6}
\contentsline {chapter}{\numberline {4}Resultados}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Desempe\IeC {\~n}o en la red}{31}{section.4.1}
\contentsline {section}{\numberline {4.2}Pruebas de \textit {scanner}}{32}{section.4.2}
\contentsline {section}{\numberline {4.3}Pruebas de radar}{35}{section.4.3}
\contentsline {section}{\numberline {4.4}Prueba en terreno}{37}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Nubes de puntos}{39}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Detecci\IeC {\'o}n de obst\IeC {\'a}culos}{40}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Comparaci\IeC {\'o}n de fuentes de datos}{44}{subsection.4.4.3}
\contentsline {chapter}{\numberline {5}Conclusiones}{47}{chapter.5}
\contentsline {section}{\numberline {5.1}Trabajos futuros}{48}{section.5.1}
\select@language {spanish}
\select@language {spanish}
\select@language {spanish}
\select@language {spanish}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{50}{chapter*.7}
\contentsline {chapter}{Anexos}{51}{chapter*.8}
\contentsline {section}{A. Interfaz a ROS del cliente implementado}{51}{section*.9}
\contentsline {section}{B. Diferencias encontradas entre \cite {martinez2012} y el verdadero protocolo del radar}{53}{section*.13}
