\beamer@endinputifotherversion {3.10pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Context and motivation: Why this project}{2}{0}{1}
\beamer@sectionintoc {2}{Objectives}{5}{0}{2}
\beamer@sectionintoc {3}{Background: Radar, ROS}{8}{0}{3}
\beamer@sectionintoc {4}{Implementation: Architecture and problems}{11}{0}{4}
\beamer@sectionintoc {5}{Tests and results: Radar, scanner, point clouds, target detection}{18}{0}{5}
\beamer@sectionintoc {6}{Conclusions}{26}{0}{6}
