#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

def exponential(x, lambd):
	if x >= 0:
		return lambd * np.exp(-lambd * x)
	else:
		return 0

def rayleigh(x, sigma):
	if x >= 0:
		return x / sigma ** 2 * np.exp(-x ** 2 / (2 * sigma ** 2))
	else:
		return 0



if __name__ == "__main__":
	plt.close("all")
	x = np.linspace(0, 10, 1000)
	p1 = map(lambda x: exponential(x, 0.5), x)
	p2 = map(lambda x: rayleigh(x, 2), x)
	plt.plot(x, p1, "r", label="Noise")
	plt.plot(x, p2, "b", label="Target")
	plt.axvline(3, color="k", linestyle="dashed", label="Threshold")
	plt.title("Probability distributions for sample's power")
	plt.xlabel("Power")
	plt.ylabel("Probability")
	plt.legend()
	plt.show()