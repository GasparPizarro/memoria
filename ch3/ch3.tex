\chapter{Diseño e implementación}
	En este capítulo se muestran los requerimientos específicos del software implementado, las decisiones de diseño que se tomaron para cumplir esos requerimientos y las características del software implementado.
	\section{Requerimientos}
		Después de analizar los objetivos de la sección \ref{objetivos}, se tiene que el software a implementar debe poder hacer lo siguiente:
		\begin{enumerate}
			\item Controlar los parámetros del radar, a decir:
			\begin{enumerate}
				\item El modo de procesamiento del radar.
				\item Los parámetros de la transformada de Fourier que se aplica a los datos.
			\end{enumerate}
			\item Controlar los parámetros del \textit{scanner}, a decir:
			\begin{enumerate}
				\item Los parámetros del controlador PID.
				\item La posición y velocidad del \textit{scanner}.
			\end{enumerate}
			\item Guardar y cargar configuraciones en el radar.
			\item Procesar los datos generados por el radar con un procesador CFAR.
			\item Permitir la visualización de los datos que genera el radar.
			\item Permitir la comunicación del software implementado con otros nodos de ROS.
		\end{enumerate}
	\section{Decisiones de diseño}
		\label{decisiones_priori}
		\subsection{Lenguajes de programación}
			La plataforma ROS tiene dos lenguajes principales, Python y C++. Python tiene la ventaja de su expresividad, esto es que se pueden hacer tareas complejas con pocas líneas de código, mientras que C++ es un lenguaje altamente eficiente, de forma que se puede hacer procesamiento de datos rápidamente.
			Se escogió trabajar con el lenguaje Python para las comunicaciones del radar, porque es más fácil de programar, y porque en esta parte no se requiere alto desempeño, al ser esta parte del software limitada por la entrada de la red. También, por el mismo motivo de simplicidad y desempeño, se escogió Python para implementar la interfaz gráfica del sistema. No obstante, se decidió implementar la funcionalidad de generación de nubes de puntos y detección de obstáculos en C++, ya que esta sección debe ser de alto desempeño, y porque la librería usada para generar los datos a visualizar, que se verá posteriormente, está hecha para C++.
		\subsection{Interfaz a ROS}
			Como se vio en la sección \ref{robot_operating_system}, ROS ofrece dos formas de comunicación entre nodos, con mensajes y con servicios. Por otro lado, el radar tiene dos tipos de comunicación.
			\begin{itemize}
				\item Los datos que el radar genera, que van desde el radar hacia el cliente.
				\item Las acciones de configuración del radar, que van desde el cliente hacia el radar.
			\end{itemize}

			Analizando las herramientas de comunicaciones en ROS, las decisiones tomadas para la interfaz del sistema fueron las siguientes:

			Servicios para los mensajes de control del radar, porque, al ser los servicios mensajes pequeños en comparación con los que envía el radar y ser usados con baja frecuencia, no se necesita que los nodos estén permanentemente consultando si se ha enviado una señal de control, como pasaría si se usaran mensajes y tópicos. Sin embargo, el radar no envía ninguna clase de realimentación acerca de cómo se han recibido los datos, de forma que solamente se usan los servicios como solicitudes, sin utilizar las respuestas que incorporan los servicios. Las definiciones de servicios son directas del protocolo \cite{martinez2012} y se mapean con los mensajes de control del radar. De esta forma, la serialización que ofrece ROS por defecto permite que un mensaje serializado pueda ser enviado por la red sin mayor procesamiento dentro del paquete. No obstante, un paquete serializado por ROS no está listo para ser enviado por la red. Se debe también agregar las cabeceras y el \textit{checksum} antes de poder enviarlo por la conexión TCP.
		
			Tópicos para los datos enviados por el radar, ya que, siendo estos mensajes los más pesados del sistema, es necesario que los nodos estén constantemente obteniendo mensajes del radar y publicándolos como mensajes de ROS. Las definiciones de mensajes (en la carpeta \texttt{msg}) no reflejan exactamente la estructura de los paquetes, ya que los paquetes enviados por el radar tienen datos inútiles para el sistema, es decir el usuario del paquete, como el \textit{checksum} y el largo de las listas de submensajes.

		\subsection{Interfaz gráfica}
			El sistema requiere una interfaz gráfica. Dado que son relativamente pocas las acciones a hacer con esta interfaz, se prefirió la simplicidad de programación por sobre la cantidad de características ofrecidas por la librería. Se analizaron dos librerías:
			\begin{description}
				\item[Tkinter \cite{tkinter}:] Esta es la librería estándar (de facto) de interfaces gráficas para Python, basada en el \textit{toolbox} Tk del lenguaje Tcl. Viene por defecto con Python.
				\item[WxPython \cite{wxpython}:] Esta es una librería de interfaces gráficas basada en WxWidgets, una librería de interfaces gráficas para C++. Se debe instalar aparte del intérprete de Python.
			\end{description}
			Considerando que la interfaz no es crítica para el sistema, para escoger se programó un bosquejo de la interfaz gráfica en ambos lenguajes y se vio cuál era más cómoda de programar. Con ello se escogió WxPython.
		
		\subsection{Visualización de datos}
			El sistema tiene que poder hacer visualización de los datos que genera el radar. Para esto se tienen dos opciones:
			\begin{itemize}
				\item Tomar los datos que emite el radar directamente y hacer un visualizador para estos.
				\item Adaptar los datos que genera el radar a un formato visualizable por Rviz.
			\end{itemize}
			En este caso es más simple adaptar los datos para Rviz. Para esto, se usó el formato de nubes de puntos, que es el mismo formato que usa el sensor Kinect \cite{ros:kinect}, usando la librería \textit{Point Cloud Library} (PCL) \cite{Rusu_ICRA2011_PCL}.


	\section{Estructura general del software}
		La librería implementada en esta memoria es, en su conjunto, un cliente que se conecta al radar para obtener mediciones de éste y enviarle señales de control. Por esto la librería se le llama ``cliente'' en otras partes de esta memoria. La librería se implementó siguiendo una estructura de nodos que refleje a la de los servidores del radar. Es decir, se implementó un nodo para el \texttt{RadarServer}, uno para el \texttt{ScannerServer} y uno para el \texttt{ScanningRadarServer}, llamados \texttt{radar\_server}, \texttt{scan\-ner\_ser\-ver} y \texttt{scanning\_\-radar\_server}, respectivamente. El trabajo de cada uno de estos nodos es responder a las llamadas de los servicios de control del radar, generando los paquetes de control especificados en el protocolo \cite{martinez2012}, a la vez que leer continuamente los datos que envía el radar, procesarlos y publicar mensajes en los tópicos correspondientes. Esta estructura se muestra en la Figura \ref{estructura_general}.
		\begin{figure}[H]
			\centering
			\includegraphics[width=15cm]{ch3/figures/estructura_general.pdf}
			\caption{Estructura general de los nodos implementados y su interfaz a ROS.}
			\label{estructura_general}
		\end{figure}
		Esta estructura muestra las tres áreas de funcionamiento de los nodos:
		\begin{itemize}
			\item Nodos de comunicaciones: Estos son \texttt{radar\_server}, \texttt{scanner\_server} y \\ \texttt{scanninng\_radar\_server}, los nodos que establecen y mantienen la conexión TCP entre el radar y el computador cliente, y que se encargan de ofrecer los servicios de configuración y los tópicos de datos.
			\item Nodos de procesamiento y visualización: Estos son \texttt{clouds} y \texttt{cfar\_processor}.
			\item \texttt{gui}. Este nodo es la interfaz gráfica del sistema.
		\end{itemize}
		También se implementó el módulo \texttt{common}, que no es un nodo de ROS, sino que es una librería con algunas funciones necesarias por los tres nodos de comunicaciones, como la envoltura de paquetes con cabecera y \textit{checksum} y los formatos de los paquetes. 
	
	\section{Decisiones incidentales}
	 	En la sección \ref{decisiones_priori} se muestran las decisiones tomadas antes de la implementación de la librería. En esta sección se muestran los problemas que aparecieron en la implementación y cómo se abordaron.

		\subsection{Lectura y escritura de mensajes}
			
		En Python, una forma usual de convertir flujos de datos en bytes a datos con significado para el programa es usar el módulo \texttt{struct}, que permite conversiones directas entre datos serializados y datos usables por un programa. En este caso, los paquetes son de largo variable, ya que las configuraciones del radar definen la cantidad de datos que se envían por mensaje.

		Dado esto, se requirió implementar una forma de parsear paquetes que permitiera paquetes de largo variable. Se implementó la clase \texttt{NetParser}, en el módulo \texttt{common}, que permite la conversión de listas de datos de largo variable, en tanto se tenga previamente el largo del paquete a convertir. Esta clase permite la descripción de una cadena de bytes como una cabecera, una lista de largo variable y una cola, cada una de ellas con un formato característico. El largo de la lista está determinado por uno de los datos obtenidos de la cabecera, permitiendo así que, dada una cadena, se puedan obtener un número indeterminado de datos de ésta.

		Para las señales de control, que van de los nodos al radar, se hizo una función en el módulo \texttt{common} que envuelve un mensaje (que en este caso sería la solicitud de un servicio) de ROS en las cabeceras y colas correspondientes, y se envía el paquete. Definiendo los campos de los servicios en el mismo orden que el protocolo, basta solamente la serialización ofrecida por ROS para que los mensajes sean serializados en el orden y con los tamaños correctos.

		\subsection{\textit{Sockets} y sincronización}
			Un motivo para decidir que los nodos estuvieran organizados de la misma forma que los servidores del radar fue el hecho de que de esta forma, cada nodo tendría un \textit{socket}, y así no habría que tener más de un nodo usando un \textit{socket}. Entonces, para cada nodo se definió una variable global (a nivel de módulo) para el \textit{socket}, de forma que cada nodo crea la conexión al iniciarse, mientras que los \textit{handlers} de los servicios y el hilo de lectura de mensajes usan el \textit{socket}. Ahora, como en cada nodo se tiene un hilo leyendo datos del radar continuamente, se necesita una forma de sincronización que permita a los servicios usar la conexión para enviar sus datos. Para ello se agregó como variable global un \texttt{Lock} asociado al \textit{socket} del nodo. De esta forma, el hilo de lectura de mensajes, cuando va a leer un mensaje, toma el \texttt{lock}, usa el \textit{socket}, y después de que lee los datos, lo libera, mientras los \textit{handlers} de los servicios hacen lo mismo.	

		\subsection{Robustez de la conexión}
			En las primeras implementaciones del sistema, éste fallaba por problemas de conexión con el radar. Se vio que, en ciertos momentos, al hacer cambiar de modo de procesamiento de datos al radar, éste reiniciaba la conexión. Como las primeras implementaciones no eran capaces de detectar este problema, entonces, al reiniciarse la conexión, el nodo \texttt{radar\_server} se caía completamente, en momentos aleatorios. Esto se compensó agregando un chequeo en las lecturas de datos, que reinicie la conexión en caso de que no se tengan datos en la conexión actual. Un problema que esto puede causar es que se podría perder sincronía en los paquetes, es decir, que al reconectarse, se comiencen a recibir paquetes que no comiencen con las cabeceras correctas. Sin embargo, en las pruebas realizadas, se vio que el radar siempre envía paquetes con las cabeceras correctas, sea cual sea el momento de la conexión, entonces el reinicio de la conexión no genera problemas de sincronización de paquetes.

		\subsection{Parámetros del radar y configuración}
			Como el radar no da información de sus parámetros directamente, es necesario que, entre un uso y otro del radar, se guarde el estado\footnote{Por ``estado'' se hace referencia a la rapidez y posición del \textit{scanner}, los parámetros del controlador PID, y el modo de procesamiento y los parámetros de la transformada de Fourier del radar.} de éste. Para ello, se usó el servidor de parámetros de forma que, cada vez que un servicio es llamado, se pone en el servidor de parámetros la configuración deseada. Estos parámetros son guardados en un archivo de configuración, el cual es cargado en el servidor de parámetros cuando inicia el cliente, antes de que inicien los nodos. Cuando los nodos inician, éstos toman los parámetros que están definidos y llaman automáticamente sus servicios con esos parámetros, de forma que al iniciar el cliente, éste configura el radar inmediatamente, poniendo el radar en el mismo estado que estaba cuando se usó por última vez.

		\subsection{Medición de datos desde el radar}
			El protocolo de comunicación del radar no especifica ninguna clase de realimentación para las señales de control, de forma que, aunque el cliente emita el paquete correcto, no es posible saber directamente si el paquete fue procesado en el radar o siquiera recibido. La única realimentación que provee el radar está en los mismos datos que este envía, que son:
			\begin{itemize}
				\item El modo de procesamiento del radar, que se puede obtener del campo \texttt{message\_id} de los mensajes.
				\item El largo de la transformada de Fourier utilizada, que se puede obtener del largo de los mensajes en modo de espectro de frecuencia.
				\item El ángulo del \textit{scanner}, que se puede obtener de los mensajes del \texttt{scanner\_server}.
				\item La rapidez del \textit{scanner}, que se puede obtener de los mensajes de \texttt{scanner\_server}.
			\end{itemize}
			También hay datos que se pueden ver a partir de las trayectorias del radar, como los cambios en el control PID del \textit{scanner}, pero que no se envían explícitamente por la conexión. Para poder obtener esos datos del radar, se hizo una suscripción temporal a los tópicos del radar y el \textit{scanner} para poder obtener toda la información posible de los datos enviados en ese momento y ponerla en el servidor de parámetros. Es importante remarcar que el radar no envía toda la información automáticamente: Por ejemplo, cuando el radar está en modo de procesamiento de \textit{peaks}, éste no entrega información sobre el largo de la transformada de Fourier, con lo que no es posible obtener todo el estado del radar en todo momento.
	
	\section{Funcionalidades implementadas}
		\subsection{Nodos de comunicaciones}
			Tres nodos fueron implementados para la funcionalidad de comunicaciones: \texttt{radar\_server}, \texttt{scanner\_server} y \texttt{scanning\_radar\_server}, que cumplen las siguientes funciones:
			\begin{itemize}
				\item Responder a las llamadas a los servicios, que son básicamente tomar el mensaje del servicio, envolverlo en con la cabecera y cola correspondiente, y enviarlo a través de la conexión establecida.
				\item Leer los datos enviados por el radar y publicarlos en los tópicos correspondientes. Para ello el procedimiento es el siguiente:
				\begin{enumerate}
					\item Se lee una cabecera, de tamaño fijo.
					\item De la cabecera, se obtiene el largo del paquete.
					\item Se obtienen los datos del paquetes.
					\item Se obtiene la cola del paquete, de largo fijo. A partir de este momento el algoritmo trabaja con una cadena fija, y no es necesario leer más de la conexión.
					\item Se verifica el \textit{checksum} del paquete. Si esta correcto, se continúa, sino, se descarta el paquete y se vuelve al inicio.
					\item Se decide el tipo del mensaje. Con esto, se despacha al \texttt{NetParser} correspondiente.
					\item Dado el tipo del mensaje, se publica en el tópico correspondiente.
				\end{enumerate}
			\end{itemize}
			ROS ofrece paralelismo en los servicios, de forma que solo es necesario implementar las funciones, y la plataforma se encarga de lanzar un hilo cuando un servicio es llamado. Sin embargo, la función que lee datos de la conexión se lanza en un hilo separado, ya que este hilo está funcionando constantemente, al contrario de los \textit{handlers} de servicios.
			
		\subsection{Interfaz gráfica}
			A pesar de que el radar es completamente controlable por línea de comandos con el programa \texttt{rosservice}, que permite llamar servicios directamente, se hace necesario una interfaz que permita configurar el radar de una forma más simple. Se creó una interfaz gráfica para el control del radar que permite configurar todos los detalles de éste. Esta interfaz se muestra en la Figura \ref{radar_gui}.
			\begin{figure}[H]
				\centering
				\subfigure[Controles del radar]
				{
					\includegraphics[width=7.5cm]{ch3/figures/gui_radar.png}
					\label{gui_radar}
				}
				\subfigure[Controles del \textit{scanner}]
				{
					\includegraphics[width=7.5cm]{ch3/figures/gui_scanner.png}
					\label{gui_scanner}
				}
				\subfigure[Menu principal]
				{
					\includegraphics[width=7.5cm]{ch3/figures/gui_menu.png}
					\label{gui_menu}
				}
				\caption{Interfaz gráfica del sistema implementado.}
				\label{radar_gui}
			\end{figure}
			Esta interfaz permite configurar todos los aspectos que ofrece el radar, esto es:
			\begin{itemize}
				\item Datos del radar, en la pestaña \texttt{Radar server}:
				\begin{itemize}
					\item El modo de procesamiento del radar.
					\item Los parámetros de la transformada de Fourier.
				\end{itemize}
				\item Datos del \textit{scanner}, para cada grado de libertad (eje) del espejo, en la pestaña \texttt{Scanner server}:
				\begin{itemize}
					\item La posición y rapidez de giro del espejo en el eje.
					\item Los parámetros del controlador PID del eje.
				\end{itemize}
			\end{itemize}
	
			Nótese que en el caso del \texttt{scanner\_server}, a pesar de que el sistema permite elegir un eje, éste solo tiene uno, con lo que solo se tiene una opción para el eje. No obstante, el protocolo \cite{martinez2012} especifica mensajes con más de un grado de libertad, y la implementación respeta esa posibilidad, permitiendo en el futuro agregar más grados de libertad al \textit{scanner} (por ejemplo, montando el radar en una plataforma giratoria) sin modificaciones mayores al programa.
	
			El menú \texttt{Settings} ofrece tres funcionalidades:
			\begin{itemize}
				\item Cargar un archivo de configuración. Esto carga un archivo \texttt{yaml} en el servidor de parámetros y luego llama a los servicios de los nodos de comunicaciones con los nuevos parámetros para fijarlos en el radar.
				\item Guardar configuración. Esto toma todos los parámetros del servidor de parámetros y los pone en un archivo \texttt{yaml}.
				\item Obtener datos del radar. Esto toma todos los datos que puede del estado del radar y los pone en la interfaz gráfica.
			\end{itemize}
			
			Cuando la interfaz gráfica se cierra, ésta guarda automáticamente todos los parámetros del radar en un archivo ubicado en \texttt{config/acumine\_radar.yaml}, independientemente de que se haya usado la funcionalidad de guardar configuración. De esta forma es posible obtener éstos al inicio del programa.
			Es importante notar que la interfaz gráfica se comunica con el radar mediante llamadas a servicios de los nodos de comunicaciones, de la misma forma que se puede hacer mediante línea de comandos con el programa \texttt{rosservice}. Es decir que las comunicaciones de la interfaz gráfica son hechas puramente con ROS. Finalmente, por lo mismo, se tiene que este nodo es prescindible para el funcionamiento del sistema, ya que todas las llamadas a servicios se pueden hacer con \texttt{rosservice}.
	
		\subsection{Nubes de puntos}
			Usando la estructura de mensajes definida por los nodos de comunicaciones, se implementó el nodo \texttt{clouds}, que recibe mensajes del nodo \texttt{scanning\_radar\_server}, acumulándolos hasta que el \textit{scanner} completa una vuelta, y convirtiéndolos a una nube de puntos que representa la vista completa del radar. De esta forma, se pueden trabajar los datos de forma estándar, pudiéndose usar las funcionalidades de PCL con los datos del radar. Además, en este formato se pueden ver fácilmente los datos con la herramienta Rviz. Nótese que el nodo \texttt{clouds} no lee directamente de la conexión TCP con el radar, sino que lee de los nodos de comunicaciones. Esto permite separar el proceso de adquisición de datos crudos y el procesamiento de estos como nubes de puntos, de forma que es posible tomar datos con el radar y luego, con Rosbag, reproducirlos y convertirlos a nubes de puntos para su procesamiento. También, en caso de que el radar no esté en movimiento, este nodo publica nubes de puntos correspondientes al ángulo al que el \textit{scanner} esté mirando.
	
		\subsection{Detección de obstáculos}
			Para la funcionalidad de detección de obstáculos, se implementó el nodo \texttt{cfar\_processor}, que aplica un procesador CFAR a una nube de puntos. Para esto se usó la librería rápida de vecinos más cercanos aproximados (\textit{Fast Library for Approximate Nearest Neighbors}, FLANN), que forma parte de PCL. Esta librería ofrece una estructura de datos que permite hacer una búsqueda de vecinos más cercanos usando \textit{kd-trees}, lo cual es básicamente una forma de encontrar puntos vecinos usando divisiones del espacio donde estos se encuentran. Con esto, esta implementación permite que el test del procesador CFAR tome puntos en un anillo tridimensional, al contrario de las implementaciones estándar de CA-CFAR, que solo toman un \textit{A-scope} (unidmensional), permitiendo así que el test que se hace para cada punto considere más información, con lo que se obtiene una mejor estimación de los parámetros de la distribución estadística del ambiente. La salida de este nodo son nubes de puntos de obstáculos, de la misma forma que se puede hacer cuando el radar está en modo de \textit{peaks}. Sin embargo, el valor de intensidad de los puntos de las nubes que publica este nodo no es la intensidad del espectro para el punto, sino que la probabilidad de detección de cada punto. También, para efectos de comparación, se implementó una versión de CFAR unidimensional.
		
		\subsection{Lanzadores}
			Considerando que todos los nodos implementados son programas ejecutables por si mismos, se vuelve complicado iniciar el sistema completo, con las funcionalidades necesarias para lo que se quiere hacer. Para facilitar el uso de los nodos implementados, se crearon los siguientes lanzadores: 
			\begin{description}
				\item[\texttt{radar.launch}:] Este lanzador se encarga de lanzar los nodos de comunicaciones, junto con la interfaz gráfica. La interfaz gráfica puede ser omitida con la opción \texttt{with\_gui:=false} como argumento del lanzador. Este lanzador es básico, ya que también carga los parámetros necesarios para que los nodos funcionen, como la IP del computador del radar y el rango del sensor, los cuales son necesarios para los nodos de procesamiento y visualización.
				\item[\texttt{clouds.launch}:] Este lanzador se encarga de lanzar los nodos de procesamiento de los datos. Además lanza una instancia de Rviz que permite visualizar las nubes de puntos cuando se reciben.
			\end{description}

	\section{Modos de uso del sistema}
		Los lanzadores descritos en la sección anterior fueron pensados para usos como éstos:
		\begin{itemize}
			\item \textbf{Configuración de parámetros}
			\begin{enumerate}
				\item Se llama al lanzador \texttt{radar.launch}.
					\begin{verbatim}
						>roslaunch hssradar_toolbox radar.launch
					\end{verbatim}
				\item Se configura el modo de procesamiento, los parámetros de la transformada de Fourier, la posición o rapidez del \textit{scanner}, etc.
				\item Se cierra la interfaz gráfica, cerrando así todo el sistema y guardando la configuración en la ubicación por defecto, o se usa la funcionalidad de guardar configuración para guardar los parámetros del radar en otro archivo.
			\end{enumerate}
			\item \textbf{Captura de datos}
			\begin{enumerate}
				\item Se llama al lanzador \texttt{radar.launch}.
					\begin{verbatim}
						>roslaunch hssradar_toolbox radar.launch
					\end{verbatim}
				\item Se configura el sistema, con un archivo de configuración o usando la interfaz gráfica.
				\item Se graban datos con Rosbag.
					\begin{verbatim}
						>rosbag record /acumine_radar
					\end{verbatim}
			\end{enumerate}
			\item \textbf{Captura y visualización de datos}
			\begin{enumerate}
				\item Se llama al lanzador \texttt{radar.launch}.
					\begin{verbatim}
						>roslaunch hssradar_toolbox radar.launch
					\end{verbatim}
				\item Se configura el radar y se deja el \textit{scanner} girando a rapidez fija.
				\item Se llama al lanzador \texttt{clouds.launch}.
					\begin{verbatim}
						>roslaunch hssradar_toolbox clouds.launch
					\end{verbatim}
				\item Se visualizan los datos con Rviz.
				\item Se graban datos con Rosbag.
					\begin{verbatim}
						>rosbag record /acumine_radar
					\end{verbatim}
			\end{enumerate}
		\end{itemize}

		Con estos modos se hace uso de todas las características del software implementado, cubriendo todos los requerimientos del software.
